<?php
/**
 * Created by PhpStorm.
 * User: coltluger
 * Date: 28/11/17
 * Time: 11:44
 */

namespace BlogBundle\Service;

use BlogBundle\Entity\Post;
use BlogBundle\Form\PostType;
use Symfony\Component\Form\FormFactory;

class PostManagerService
{
    const LIMIT_POSTS = 10;

    /** @var FormFactory */
    private $formFactory;

    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function getForm()
    {
        return $this->formFactory->create(PostType::class, null, []);
    }
}