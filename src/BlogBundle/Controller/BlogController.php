<?php
/**
 * Created by PhpStorm.
 * User: coltluger
 * Date: 27/11/17
 * Time: 09:51
 */

namespace BlogBundle\Controller;

use BlogBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Response;


class BlogController extends Controller
{
    public function homeAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('BlogBundle:Post');

        $posts = $repository->findAll();

        $parameters = ['posts' => $posts];

        return $this->render('@Blog/Home/home.html.twig', $parameters);
    }

    public function showAction($id): Response
    {
        $em = $this->getDoctrine()->getManager();

        $post = $em->getRepository('BlogBundle:Post')->find($id);

        if (!$post){
            throw $this->createNotFoundException('Unable to find Blog post.');
        }

        return $this->render(':partials:show-post.html.twig', array('post' => $post));
    }

    public function createPostAction(Request $request)
    {
        $form = $this->get('blogbundle.service.postmanager')->getForm();

        $form->handleRequest($request);

        if ($form->isValid()){
            /** @var Post $post */
            $post = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return new Response('Thanks for creating a new post, bitch !');
        }

        $parameters = ['postForm' =>  $form->createView()];

        return $this->render(':partials:new-post.html.twig', $parameters);
    }

    public function newPostAction()
    {
        $form = $this->get('blogbundle.service.postmanager')->getForm();

        $parameters = ['postForm' => $form->createView()];

        return $this->render('@Blog/forms/new-post.html.twig', $parameters);
    }

}
