<?php
/**
 * Created by PhpStorm.
 * User: coltluger
 * Date: 24/11/17
 * Time: 11:03
 */

namespace UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $lastName;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     */

    public function setFirstName($firstName): void
    {
        $this->first_name = $firstName;
    }

    /**
     * Get firstName
     *
     * @return string
     */

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     */

    public function setLastName($lastName): void
    {
        $this->last_name = $lastName;
    }

    /**
     * Get lastName
     *
     * @return string
     */

    public function getLastName():  ?string
    {
        return $this->lastName;
    }
}
